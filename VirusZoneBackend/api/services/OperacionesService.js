module.exports={
  /*
    metodo encargado de calcular la mediana
    @params lista de elementos a los cuales se le deba calcular la mediana y nombre de la variable
    por la cual se calcula la mediana
    @return valor de la mediana para la lista de elementos ingresada

  */
mediana : function(lista,variable){
     var mediana=0;
    lista.sort(((a, b) => a[variable]- b[variable]));
    if(lista.length%2==0){
        const posicion=Math.floor(lista.length/2)-1;
        const valor1=lista[posicion][variable];
        const valor2=lista[posicion+1][variable];
        
      mediana=(valor1+valor2)/2; 
      return mediana;
    }
    else{
      const pos=Math.trunc(lista.length/2);
        mediana=lista[pos][variable];
       return mediana;
        
    }
  
},
/*
metodo encargado de calcular la desviacionestandar
@params lista de elementos a los cuales se le deba calcular la desviacion estandar
,promedio de la variable que se este calculando,nombre de la variable a la cual se le esta calculando la
desviacion estandar 
@return valor de la desviacion estandar para la lista de elementos ingresada
*/
desviacionestandar: function(lista,promedio,variable){
  var dividendo=0;
   lista.forEach(function(element,index,array){
    const suma=element[variable]-promedio;
    const alcuadrado=Math.pow(suma,2);
    dividendo=dividendo+alcuadrado;
    });
const divisor=lista.length-1;
const division=dividendo/divisor;
const desviacionestandar=Math.sqrt(division);
return desviacionestandar;
},

/*
Metodo que calcula el promedio para un conjunto de elementos
@params lista que contiene los elementos a los cuales se le calculara el promedio
@return el promedio para el conjunto de numeros ingresado
*/
promedio: function(lista,valor){
  var edadtotal = lista.reduce((sum, value) => ( sum + value[valor]), 0);
  const promedioedad=edadtotal/lista.length;
  return promedioedad;
},
/*
metodo encargado de calcular el rango_intercuartilico de un conjunto de elementos
@params lista que contiene los elementos a los cuales se les calculara el rango intercuartilico y
la variable por la cual se va a realizar el calculo
@return el rango intercuartilico para la lista ingresada
*/
rango_intercuartilico: async function(lista,valor){
    if(lista.length%2==0){
         const posicion=(lista.length/2);
         const lista1=  lista.slice(0 , posicion);
         const lista2= lista.slice((posicion),lista.length);
         const mediana1=this.mediana(lista1,valor);
         const mediana2=this.mediana(lista2,valor);
         const rangointercuartilico=mediana2-mediana1;
         
     return rangointercuartilico;
            }
    else{
         const posicion=(lista.length/2);
         const lista1= lista.slice(0 , posicion);
         const lista2= lista.slice((posicion+1),lista.length);
         const mediana1=this.mediana(lista1,valor);
         const mediana2=this.mediana(lista2,valor);
         const rangointercuartilico=mediana2-mediana1;
     return rangointercuartilico;
            }
}


};