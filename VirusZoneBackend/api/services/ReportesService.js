var moment = require('moment');
module.exports={
  /*
  metodo al cual le ingresa el numero del mes y devuelve el nombre del mes
  @params lista con el numero que le corresponde a unos meses
  @return lista con el nombre de los meses ingresados
  */
    meses: function(lista){
  
        meses={
          1:"Enero",
          2:"Febrero",
          3:"Marzo",
          4:"Abril",
          5:"Mayo",
          6:"Junio",
          7:"Julio",
          8:"Agosto",
          9:"Septiembre",
          10:"Octubre",
          11:"Noviembre",
          12:"Diciembre",
          13:"No Registra"
        }
      
        for ( i =0; i< lista.length; i++) {
            lista[i].mes= meses[lista[i].mes]     
         
      
        }
        return lista ;
      },
      /*
      metodo que se encarga de construir la cadena de los rangos ingresados en los reportes
      @params valorminimo, valor maximo, signos si es <=,< o >,>= y un booleano si es un tipo fecha
      @return cadena para hacer la consulta del reporte 
      */
      ComparacionesRango: function(variablemin,variablemax,compmenor,compmayor,fecha){
        const filtro={ };
        if (variablemin !== undefined || variablemax != undefined) {
          
          if (variablemin !== undefined) 
          {
            if(fecha==true){
              filtro[compmenor] = new Date(moment(variablemin,"DD/MM/YYYY"));  
            
          }
          else{
            filtro[compmenor] = variablemin;
          }
        }
           if(variablemax!== undefined)
           { 
             if(fecha==true){
              filtro[compmayor] = new Date(moment(variablemax,"DD/MM/YYYY"));
             
            }else
               {
                 filtro[compmayor] = variablemax;
                }
          } 
           return filtro;
      
      }
    }



};
