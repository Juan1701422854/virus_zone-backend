module.exports={
	/*
	metodo encargado de limpiar los caracteres especiales en la base de datos
	*/
    reemplazarAcentos: function(cadena)
{
  
	var chars={
		"á":"a", "é":"e", "í":"i", "ó":"o", "ú":"u",
		"à":"a", "è":"e", "ì":"i", "ò":"o", "ù":"u", "ñ":"n",
		"Á":"A", "É":"E", "Í":"I", "Ó":"O", "Ú":"U",
		"À":"A", "È":"E", "Ì":"I", "Ò":"O", "Ù":"U", "Ñ":"N"}
  var expr=/[áàéèíìóòúùñ]/ig;
  var res=cadena.replace(expr,function(e){return chars[e]});
	return res;
},
/*
metodo encargado de quitar los espacios despues de el punto en algunas comunas de caminos donde varian
@return retorna cadena sin espacios despues de los puntos
*/
reemplazarespacios:function(cadena){
  var res=cadena.replace('. ','.');
  return res;
}

};

