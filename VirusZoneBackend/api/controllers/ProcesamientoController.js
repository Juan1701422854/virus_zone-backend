1/**
 * ProcesamientoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const request = require('request');
var moment = require('moment');
const comunaJson = require('../services/barriosGeoreferenciados');
/*
funcion para calcular la oportunidad de cada uno de los portadores
@params fecha minima de sintomas y fecha maxima de sintomas
@return oportunidad del paciente
*/

function obtenerOportunidad(fechaMaxima,fechaMinima){
    if (isNaN(fechaMaxima) || isNaN(fechaMinima)) {
        return -1;
    }
    var oportunidad = (Math.round((new Date(fechaMaxima) - new Date(fechaMinima))/ (1000*60*60*24)));
    return oportunidad;
  }
  /*
Metodo que valida si es un numero
@params numero
@return el numero si es numero de lo contrario retorna -1
*/

function esNumerico(numero){
    if (!isNaN(parseFloat(numero)) && isFinite(numero)) {
        return numero
    }
    else{
        -1
    }
}

function callback(error, response, body) {
  if (!error && response.statusCode == 200) {
    const info = JSON.parse(body);
    for(var index in info){
        Portador.create(info[index]).fetch().exec(
            function (error) {
                if (error) {
                    return "Error al ingresar los datos"
                }
            }
            
        );
    }
    sails.log.debug("Total Documentos Subidos: "+info.length);
    
  }
}
/*
metodo encargado de llenar la base de datos
@params link del api donde consumimos los datos
*/
async function callback2(error, response, body) {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body);
      var iterador = 0;
      for(var index in info){
            await Portador.createEach([{ 
                semana: info[index].semana,
                edad_: info[index].edad_,
                uni_med_: info[index].uni_med_,
                sexo_: info[index].sexo_,
                nombre_barrio: info[index].nombre_barrio,
                comuna: info[index].comuna,
                tip_ss_: info[index].tip_ss_,
                cod_ase_: info[index].cod_ase_,
                fec_con_: info[index].fec_con_,
                ini_sin_: info[index].ini_sin_,
                fechaConsultaNumerico: esNumerico(new Date(moment(info[index].fec_con_,"DD/MM/YYYY"))),
                fechaSintomaNumerico: esNumerico(new Date(moment(info[index].ini_sin_,"DD/MM/YYYY"))),
                tip_cas_: info[index].tip_cas_,
                pac_hos_: info[index].pac_hos_,
                oportunidad: obtenerOportunidad((moment(info[index].fec_con_,"DD/MM/YYYY")),
                    moment(info[index].ini_sin_,"DD/MM/YYYY"))
              
              }]);
          iterador = iterador +1;
      }
      sails.log.debug("Total Documentos Subidos: "+info.length);
      
    }
  }

  


module.exports = {
    get: function(req, res){
        Portador.find()
            .then(function(portadores){
                if (!portadores || portadores.length === 0) {
                    return res.send({
                        'success': false,
                        'menssage': 'No hay datos'
                    })
                }

                return res.send({
                    'success': true,
                    'menssage': 'He obtenido los datos de manera correcta',
                    'data': portadores
                })
            })
            .catch(function(err){
                sails.log.debug(err)
                return res.send({
                    'success': false,
                    'menssage': 'Error no se puedo obtener ningun registro'
                })
            })

    },

    PostCreate : function(req, res) {
        sails.log.debug(req.allParams());
        Portador.create(req.allParams())
            .then(function (portadores) {
                return res.send({
                    'success': true,
                    'menssage': 'Se ha creado el registro satisfactoriamente'
                })
            })
            .catch(function (err) {
                sails.log.debug(err)
                return res.send({
                    'success': false,
                    'menssage': 'Error, NO se creo el registro'
                })
                
            })
    },

    apiDataBase : function(req, res) {
        const url = {
            url: req.body.url,
            headers: {
              'User-Agent': 'request'
            }
          };
        
        request(url, callback2);
        return res.send({
            'success': true,
            'menssage': 'Se estan importando los datos.....'
        });
        
    },

    save : function(req, res) {
        sails.log(req.body);
        Portador.create(req.body).fetch().exec(
            function (error,response) {
                console.log(response);
                return res.json(response);
            }
        );
    },
    
    Prueba : async function(req, res) {
        let sexos;
        await Portador.native(async function(err,portador){
           sexos = await portador.distinct("comuna");
          });
            let out = {};
            for (var i = 0; i < sexos.length; i++) {              
                let constante = await  Portador.count({comuna: sexos[i]});
                // out[i] ={};
                // out[i]["sexo"] = sexos[i];
                // out[i]["cout"] = constante;
                out[sexos[i]] = constante;
            } 
        return res.json(out);
    },
    GetTotalComuna : async function(req, res) {
        let comunas;
        await Portador.native(async function(err,portador){
           comunas = await portador.distinct("comuna");
          });
            let out = {};
            for (var i = 0; i < comunas.length; i++) {              
                let constante = await  Portador.count({comuna: comunas[i]});
                out[i] ={};
                out[i]["comuna"] = comunas[i];
                out[i]["total"] = constante;
                //out[comunas[i]] = constante;
            } 
        return res.json(out);
    },
    GetTotalbarriosXComuna : async function(req, res) {
        let comunaRequerida = req.body.comuna;
        let barrios;
        await Portador.native(async function(err,comunaEncontrada = async function(err, query){
                comunaEncontrada = await Portador.find({comuna: comunaRequerida});
            }){
            barrios = await comunaEncontrada.distinct("nombre_barrio");
          });
            let out = {};
            for (var i = 0; i < barrios.length; i++) {              
                let constante = await  Portador.count({nombre_barrio : barrios[i]});
                out[i] ={};
                out[i]["barrio"] = barrios[i];
                out[i]["total"] = constante;
                //out[comunas[i]] = constante;
            } 
        return res.json(out);
    },
/*
metodo que retorna la ubicacion con altitud y longitud de cada comuna
*/
    comunasGeoreferenciados: async function(req,res){
        var comunas;
        await Portador.native(async function(err,portador){
           comunas = await portador.distinct("comuna");
          });
            let retorno = {};
            for (var i = 0; i < comunas.length; i++) { 
                let conteo = await  Portador.count({comuna: comunas[i]});
                retorno[i] ={};
                retorno[i]["nombreComuna"] = comunas[i];
                retorno[i]["conteo"] = conteo;
                for (let indiceComuna = 0; indiceComuna < comunaJson.barrios.length; indiceComuna++) {
                    if (comunaJson.barrios[indiceComuna].nombre == comunas[i]) {
                        retorno[i]["latitude"] = comunaJson.barrios[indiceComuna].latitude;
                        retorno[i]["longitude"] = comunaJson.barrios[indiceComuna].longitude;
                    }                    
                }
                
            } 
        return res.json(retorno);
    }

    
}

