/**
 * ProcesamientoConsultasController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const serviciooperaciones=require('../services/OperacionesService');
const reporteservicio=require('../services/ReportesService');
module.exports = {
    /*
    Servicio encargado de generar todos los reportes obteniendo frecuencia, promedio, media, mediana,
    rango intercuartilico y desviacion estandar.
    @params  variables para generar reportes edad minima, edad maxima, sexo, fecha consulta minima,
    fecha consulta maxima, oportunidad, fecha sintomas minima, fecha sintomas maximo,comuna,barrio.  
    @return json con el resultado del reporte solicitado
    */
    Reportes: async function(req,res){
        const filtros={};
         if(req.body.sexo_!==undefined){
         filtros.sexo_= req.body.sexo_;
         }
        if(req.body.nombre_barrio!==undefined){
            filtros.nombre_barrio=req.nombre_barrio;
        }
        filtros.edad_=reporteservicio.ComparacionesRango(req.body.edad_min,req.body.edad_max,'>=','<=',false);
        filtros.oportunidad=reporteservicio.ComparacionesRango(req.body.oport_min,req.body.oport_max,'>=','<',false);
        filtros.fechaSintomaNumerico=reporteservicio.ComparacionesRango(req.body.fechasintomas_min,req.body.fechasintomas_max,'>=','<=',true);
        filtros.fechaConsultaNumerico=reporteservicio.ComparacionesRango(req.body.fechaconsulta_min,req.body.fechaconsulta_max,'>=','<=',true);
        if(req.body.comuna!==undefined){
            filtros.comuna=req.body.comuna;
        }
        if(req.body.tip_ss_!==undefined){
            filtros.tip_ss_=req.body.tip_ss_;
        }
        const filtrados = await Portador.find(filtros);
        const cantidad=await Portador.count(filtros);
        var filtrosretorno=filtrados.map( portador  =>{
         return{
                 edad_: portador.edad_,
                 comuna:portador.comuna,
                 sexo_:portador.sexo_,
                 nombre_barrio:portador.nombre_barrio,
                 tip_ss_:portador.tip_ss_,
                 oportunidad:portador.oportunidad,
                 fec_con_:portador.fec_con_,
                 ini_sin_:portador.ini_sin_  
             }
        });
       if(req.body.edad==undefined & req.body.oportunidad==undefined){
            res.json({total:cantidad,listapacientes:filtrosretorno});
        }
        else if(req.body.edad==true){
            const promedioedad=serviciooperaciones.promedio(filtrosretorno,"edad_");
            const mediana=serviciooperaciones.mediana(filtrosretorno,"edad_");
            const desviacionestandar=serviciooperaciones.desviacionestandar(filtrosretorno,promedioedad,"edad_");
            const rangointercuartilico=await serviciooperaciones.rango_intercuartilico(filtrosretorno,"edad_");
           res.json({promedio:promedioedad,desviacionestandar:desviacionestandar,mediana:mediana, rango_intercuartilico:rangointercuartilico});   
        }
        else if(req.body.oportunidad==true){
            const promediooportunidad=serviciooperaciones.promedio(filtrosretorno,"oportunidad");
            const mediana=serviciooperaciones.mediana(filtrosretorno,"oportunidad");
            const desviacionestandar=serviciooperaciones.desviacionestandar(filtrosretorno,promediooportunidad,"oportunidad");
            const rangointercuartilico=await serviciooperaciones.rango_intercuartilico(filtrosretorno,"oportunidad");
            res.json({promedio:promedioedad,desviacionestandar:desviacionestandar,mediana:mediana, rango_intercuartilico:rangointercuartilico});
        }
    }
 };
    


