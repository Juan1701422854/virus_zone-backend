module.exports={
       /*
    Servicio que devuelve todas las comunas de la ciudad
    @return json con todas las comunas de la ciudad
    */
    GetComunas: async function(req,res){
        const comunas= await Portador.find();
        const comunasUnicas = new Set(comunas.map(fila => fila.comuna));
        const arreglo=Array.from(comunasUnicas)
        res.json({comunas:arreglo});  
    },
    /*
    Servicio que devuelve todas las barrios de una  comuna ingresada
    @params  nombre de la comuna de la que se desea que se retornen los barrios
    @return  json con todas los barrios de la comuna ingresada
    */
    Retornobarrios:async function(req,res){
        listaportadores=await Portador.find({comuna:req.body.comuna});
        const barrios = new Set(listaportadores.map(fila => fila.nombre_barrio));
        const arreglo=Array.from(barrios);
        arreglo.sort();
        res.json({barrios:arreglo});
    },
    /*
    Servicio que devuelve todos los barrios la ciudad
    @return json con todas los barrios de la ciudad
    */
   TodosLosBarrios:async function(req,res){
        listaportadores=await Portador.find();
        const barrios = new Set(listaportadores.map(fila => fila.nombre_barrio));
        const arreglo=Array.from(barrios);
        arreglo.sort();
        res.json({barrios:arreglo});
    }
}