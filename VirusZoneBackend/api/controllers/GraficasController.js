const ReportesService=require('../services/ReportesService');
module.exports={

    /*
    Servicio que devuelve el numero de hombres y mujeres
    @return numero de hombres y mujeres en la base de datos
    */
    GetSexo: async function(req,res){
        const nHombres = await Portador.count({ sexo_: 'M' });
        const nMujeres=await Portador.count({sexo_:'F'});

        res.json({Masculino: nHombres, Femenino: nMujeres});

    },
    /*
    Servicio que devuelve el numero de registros en un rango de edad
    @params rango de edad 
    @return json con el total de registros que esten dentro del  rango ingresado
    */
    Edad: async function(req,res){
        const TotalRangoEdades=await Portador.count({edad_: { '>=': req.body.edadinicial, '<=': req.body.edadfinal }});
        const TotalRegistros=await Portador.count();
        res.json({RangoTotalEdad: TotalRangoEdades, Total:TotalRegistros});
    },
    /*
    Servicio que envia el numero de personas contagiadas de una comuna.
    @params nombre de la comuna.
    @return numero de contagiados segun la comuna seleccionada. 
    */
    Comuna: async function(req,res){
        const TotalComuna=await Portador.count({comuna: req.body.comuna});
        const TotalRegistros=await Portador.count();
        res.json({TotalPComuna: TotalComuna,TotalCiudad: TotalRegistros})

    },
    /*
    Servicio que envia el numero de personas contagiadas de una comuna.
    @params nombre de la comuna.
    @return numero de contagiados segun la comuna seleccionada. 
    */
    NumeroCasosPorBarrio: async function(req,res){
        listaportadores=await Portador.find({comuna:req.body.comuna});
        const barrios = new Set(listaportadores.map(fila => fila.nombre_barrio));
        const arreglo=Array.from(barrios);
        arreglo.sort();
        var contador=0;
        var objeto={};
        let lista=[];
        for(var i = 0; i < arreglo.length;i++){

            contador=await Portador.count({nombre_barrio:arreglo[i]});
            objeto={nombre:arreglo[i],
                    cantidad:contador}
            lista[i]=objeto;
        }
         res.json({casos:lista})

       },
       /*
       Metodo encargado de devolver cada mes con el respectivo numero de casos
       @return json con los meses y el numero de contagios
       */
    Casospormeses: async function(req,res){
         const db= Portador.getDatastore().manager;
         var listacontagiadospormes=[];
         const variable=db.collection('portador') 
         .aggregate( [ {
             $group: {
                 _id: {$substr: ['$ini_sin_', 3,2]}, 
                 numerodecontagios: {$sum: 1}
             }
         }]).toArray((err, results) => {
             if (err) {
             sails.log("error");
             }
              listacontagiadospormes=results.map( cantidad  =>{
          
                 return{
                    mes:parseFloat(cantidad._id),
                    numerodecontagios:cantidad.numerodecontagios
                    
                 }
             });
             for(var i = 0; i< listacontagiadospormes.length;i++){
                  if(isNaN(listacontagiadospormes[i].mes)){
                      listacontagiadospormes[i].mes=13;
                  }
             }
            listacontagiadospormes.sort(((a, b) => a.mes- b.mes));
            const listacontagiadosdef= ReportesService.meses(listacontagiadospormes);
             res.json({meses: listacontagiadosdef});
             });;
         
         
         }




}