    /**
 * Portador.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    semana: { type: 'number'},
    edad_ :  { type: 'number'},
    uni_med_ :  { type: 'number'},
    sexo_ : { type: 'string'},
    nombre_barrio :  { type: 'string'},
    comuna : { type: 'string'},
    tip_ss_ :  { type: 'string'},
    cod_ase_: { type: 'string'},
    fec_con_ : { type: 'string'},
    ini_sin_ : { type: 'string'},
    fechaConsultaNumerico : { type: 'number'},
    fechaSintomaNumerico : { type: 'number'},
    tip_cas_ :  { type: 'number'},
    pac_hos_ : { type: 'number'},
    oportunidad: { type: 'number'}

  },

};

