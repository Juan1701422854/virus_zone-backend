/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/
  'GET /api/get': 'ProcesamientoController.get',
  'POST /api/PostSave': 'ProcesamientoController.save',
  'POST /api/Create': 'ProcesamientoController.PostCreate',
  'POST /api/apiDataBase': 'ProcesamientoController.apiDataBase',
  'GET /api/Sexo': 'ProcesamientoController.ConteoSexo',
  'GET /api/Prueba': 'ProcesamientoController.Prueba',
  'GET /api/getSexo': 'GraficasController.GetSexo',
  'POST /api/edad': 'GraficasController.Edad',
  'POST /api/comuna': 'GraficasController.Comuna',
  'GET /api/prueba': 'ProcesamientoController.Prueba',
  'GET /api/getcomunas': 'ObtenerValoresController.GetComunas',
  'GET /api/getcomunasV2': 'ProcesamientoController.GetTotalComuna',
  'POST /api/GetTotalbarriosXComuna': 'ProcesamientoController.GetTotalbarriosXComuna',
  'POST /api/Reportesfiltros': 'ProcesamientoConsultasController.Reportes',
  'POST /api/Obtenerbarrios': 'ObtenerValoresController.Retornobarrios',
  'POST /api/CasosPorBarrios': 'GraficasController.NumeroCasosPorBarrio',
  'GET /api/NumeroPorMes': 'GraficasController.Casospormeses',
  'GET /api/TodosLosBarrios': 'ObtenerValoresController.TodosLosBarrios',
  'GET /api/comunasGeoreferenciados': 'ProcesamientoController.comunasGeoreferenciados',
  
  
};
